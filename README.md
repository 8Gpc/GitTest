#Git 学习

##基本命令
    gti clone    克隆仓库
    git init    初始化项目
    git config    配置信息
    git remote    远端仓库管理
    git branch    分支
    git checkout    切换分支
    git merge    合并分支
    git commit    更新
    git pull    获取远端仓库数据并于本地合并
    git push    推送至远端
